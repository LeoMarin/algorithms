#pragma once
#include <iostream>

class RedBlackTree
{
private:
	struct Node
	{
		int key;
		bool isRed;
		Node* left;
		Node* right;
		Node* parent;

		Node(int newKey)
		{
			key = newKey;
			isRed = true;
			left = nullptr;
			right = nullptr;
			parent = nullptr;
		}
	};


	Node* root;

	Node* GetGrandparent(Node* node) { return node->parent->parent; }
	Node* GetUncle(Node* node) { return (node->key > GetGrandparent(node)->key) ? GetGrandparent(node)->left : GetGrandparent(node)->right; }

	void LeftRotate(Node* node);
	void RightRotate(Node* node);
	void Rebalance(Node* node);
	bool UncleIsRed(Node* node);

	void InOrderTraversal(Node* node);
	void PreOrderTraversal(Node* node);
	void PostOrderTraversal(Node* node);
public:
	RedBlackTree() { root = nullptr; }
	void CreateRedBlackTree(int* a, int size);
	void InsertElement(int key);
	void DeleteElement(int key);

	void InOrderTraversal() { std::cout << "InOrderTraversal: "; InOrderTraversal(root); std::cout << std::endl; }
	void PreOrderTraversal() { std::cout << "PreOrderTraversal: "; PreOrderTraversal(root); std::cout << std::endl; }
	void PostOrderTraversal() { std::cout << "PostOrderTraversal: "; PostOrderTraversal(root); std::cout << std::endl; }

};

void RedBlackTree::CreateRedBlackTree(int* a, int size)
{
	for (int i = 0; i < size; i++)
		InsertElement(a[i]);
}

void RedBlackTree::InsertElement(int key)
{
	Node* node = new Node(key);

	if (root == nullptr)
	{
		node->isRed = false;
		root = node;
		return;
	}

	Node* temp = root;
	while (temp != nullptr)
	{
		if (key > temp->key)
		{
			if (temp->right == nullptr)
			{
				node->parent = temp;
				temp->right = node;
				break;
			}
			temp = temp->right;
		}
		else
		{
			if (temp->left == nullptr)
			{
				node->parent = temp;
				temp->left = node;
				break;
			}
			temp = temp->left;
		}
	}

	Rebalance(node);
}

void RedBlackTree::LeftRotate(Node* node)
{
	Node* right = node->right;
	node->right = right->left;
	if (right->left != nullptr)
		right->left->parent = node;
	right->left = node;
	right->parent = node->parent;

	if (node == root)
	{
		root = right;
		right->isRed = false;
	}
	else
	{
		if (node->parent->left == node)
			node->parent->left = right;
		else
			node->parent->right = right;
	}
	node->parent = right;
}

void RedBlackTree::RightRotate(Node* node)
{
	Node* left = node->left;
	node->left = left->right;
	if(left->right != nullptr)
		left->right->parent = node;
	left->right = node;
	left->parent = node->parent;

	if (node == root)
	{
		root = left;
		left->isRed = false;
	}
	else
	{
		if (node->parent->left == node)
			node->parent->left = left;
		else
			node->parent->right = left;
	}
	node->parent = left;
}

void RedBlackTree::Rebalance(Node* node)
{
	if (node == root)
	{
		node->isRed = false;
		return;
	}

	if (node == nullptr || GetGrandparent(node) == nullptr)
		return;

	if (!(node->isRed && node->parent->isRed))
		return;

	if (UncleIsRed(node))
	{
		node->parent->isRed = !node->parent->isRed;
		GetGrandparent(node)->isRed = !GetGrandparent(node)->isRed;
		GetUncle(node)->isRed = !GetUncle(node)->isRed;
		Rebalance(GetGrandparent(node));
	}
	else if (node->key > GetGrandparent(node)->key)
	{
		if (node->key <= node->parent->key)
		{
			RightRotate(node->parent);

			node = node->right;
			node->parent->isRed = !node->parent->isRed;
			GetGrandparent(node)->isRed = !GetGrandparent(node)->isRed;
			LeftRotate(GetGrandparent(node));
			Rebalance(GetGrandparent(node));
		}
		else
		{
			node->parent->isRed = !node->parent->isRed;
			GetGrandparent(node)->isRed = !GetGrandparent(node)->isRed;
			LeftRotate(GetGrandparent(node));
			Rebalance(GetGrandparent(node));
		}
	}
	else
	{
		if (node->key > node->parent->key)
		{
			LeftRotate(node->parent);

			node = node->left;
			node->parent->isRed = !node->parent->isRed;
			GetGrandparent(node)->isRed = !GetGrandparent(node)->isRed;
			RightRotate(GetGrandparent(node));
			Rebalance(GetGrandparent(node));
		}
		else
		{
			node->parent->isRed = !node->parent->isRed;
			GetGrandparent(node)->isRed = !GetGrandparent(node)->isRed;
			RightRotate(GetGrandparent(node));
			Rebalance(GetGrandparent(node));
		}
	}
}

inline bool RedBlackTree::UncleIsRed(Node* node)
{
	if (node->key > GetGrandparent(node)->key)
		return (GetGrandparent(node)->left) ? GetGrandparent(node)->left->isRed : false;
	return (GetGrandparent(node)->right) ? GetGrandparent(node)->right->isRed : false;
}


void RedBlackTree::DeleteElement(int key)
{

}

void RedBlackTree::InOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	InOrderTraversal(node->left);
	std::cout << node->key << " ";
	InOrderTraversal(node->right);
}

void RedBlackTree::PreOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	std::cout << node->key << " ";
	PreOrderTraversal(node->left);
	PreOrderTraversal(node->right);
}

void RedBlackTree::PostOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	PostOrderTraversal(node->left);
	PostOrderTraversal(node->right);
	std::cout << node->key << " ";
}