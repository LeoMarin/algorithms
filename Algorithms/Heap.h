#pragma once
#include<vector>
#include<iostream>

class MaxHeap
{
private:
	std::vector<int> heap;

	void Heapify(int i);

	int GetLeftChildIndex(int index) { return index * 2 + 1; }
	int GetRightChildIndex(int index) { return index * 2 + 2; }
	int GetParentIndex(int index) { return (index - 1) / 2; }
	void Swap(int &a, int &b);

public:
	void BuildMaxHeap(int* a, int size);
	int PopLargestElement();
	void PrintTree();

	MaxHeap(int* a, int size) { BuildMaxHeap(a, size); }
};

void MaxHeap::Swap(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}

void MaxHeap::BuildMaxHeap(int* a, int size)
{
	heap.reserve(size);
	for (int i = 0; i < size; i++)
		heap.emplace_back(a[i]);

	for (int i = (heap.size() / 2) - 1; i >= 0; i--)
		Heapify(i);
}

void MaxHeap::Heapify(int i)
{
	int left = GetLeftChildIndex(i);
	int right = GetRightChildIndex(i);


	if (right < heap.size())
	{
		if (heap[i] >= heap[left] && heap[i] >= heap[right])
		{
			return;
		}
		else if (heap[left] >= heap[i] && heap[left] >= heap[right])
		{
			Swap(heap[i], heap[left]);
			Heapify(left);
			return;
		}
		else
		{
			Swap(heap[i], heap[right]);
			Heapify(right);
			return;
		}
	}
	else if (left < heap.size() && heap[left] >= heap[i])
	{
		Swap(heap[i], heap[left]);
		return;
	}
}

int MaxHeap::PopLargestElement()
{
	if (heap.size() == 0)
		return NULL;
	int max = heap[0];
	heap[0] = heap[heap.size() - 1];
	heap.pop_back();

	Heapify(0);
	return max;
}

void MaxHeap::PrintTree()
{
	std::cout << "\n";
	int j = 1;
	int k = 1;
	for (int i = 0; i < heap.size(); i++)
	{
		std::cout << heap[i];
		if (j == k )
		{
			std::cout << "\n";
			k*=2;
			j = 1;
		}
		else
		{
			std::cout << "\t";
			j++;
		}
	}
	std::cout << "\n";
}