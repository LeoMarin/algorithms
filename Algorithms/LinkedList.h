#pragma once
#include <iostream>


class LinkedList
{
private:
	struct Node
	{
		int data;
		Node* next;
	};
	Node* head;
	int size;
public:
	LinkedList() { head = new Node; head->data = 0; head->next = nullptr; size = 0; }
	void AddFirst(int data);
	void AddLast(int data);
	void InsertAfter(int data, int index);
	void InsertBefore(int data, int index);
	void Delete(int data);
	void PrintList();
	int Size();
};

inline void LinkedList::AddFirst(int data)
{
	Node* node = new Node;
	node->data = data;
	node->next = head->next;

	head->next = node;

	size++;
}

inline void LinkedList::AddLast(int data)
{
	Node* node = new Node;
	node->data = data;
	node->next = head->next;

	Node* temp = head;
	while (temp->next != nullptr)
	{
		temp = temp->next;
	}

	temp->next = node;

	size++;
}

inline void LinkedList::InsertAfter(int data, int index)
{
	Node* node = new Node;
	node->data = data;
	node->next = nullptr;

	int i = 0;

	Node* temp = head;
	while (temp->next != nullptr && i < index)
	{
		temp = temp->next;
		i++;
	}

	node->next = temp->next;
	temp->next = node;

	size++;
}

inline void LinkedList::InsertBefore(int data, int index)
{
	InsertAfter(data, index - 1);
}

inline void LinkedList::Delete(int data)
{
	Node* temp = head;
	while ( temp->next != nullptr )
	{
		if (temp->next->data == data)
		{
			Node* nt = temp->next;
			temp->next = temp->next->next;
			delete(nt);
			size--;
			continue;
		}
		temp = temp->next;
	}
}

inline void LinkedList::PrintList()
{

	Node* temp = head;
	while (temp->next != nullptr)
	{
		temp = temp->next;
		std::cout << temp->data << " ";
	}
	std::cout << std::endl;
}

int LinkedList::Size()
{
	return size;
}

