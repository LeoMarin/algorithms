#pragma once

template <class T>
void Swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}

template <class T>
void InsertionSort(T* a, int size)
{
	for (int i = 0; i < size; i++)
	{
		int j = i;
		while (j != 0 && a[j] < a[j - 1])
		{
			Swap(a[j], a[j - 1]);
			j--;
		}
	}
}

template <class T>
void SelectionSort(T* a, int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		int minJ = i;
		for (int j = i; j < size; j++)
		{
			if (a[j] < a[minJ])
				minJ = j;
		}
		if (minJ != i)
			Swap(a[i], a[minJ]);
	}
}

template <class T>
void BubbleSort(T* a, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = i; j < size; j++)
		{
			if (a[i] > a[j])
				Swap(a[i], a[j]);
		}
	}
}

// MergeSort helper function
template <class T>
void Merge(T* a, int low, int mid, int high)
{
	T* tempArray = new T[high - low + 1];

	int i = low;
	int j = mid + 1;
	int k = 0;

	while (i <= mid && j <= high)
	{
		if (a[i] < a[j])
		{
			tempArray[k] = a[i];
			i++;
			k++;
		}
		else
		{
			tempArray[k] = a[j];
			j++;
			k++;
		}
	}

	while (i <= mid)
	{
		tempArray[k] = a[i];
		i++;
		k++;
	}

	while (j <= high)
	{
		tempArray[k] = a[j];
		j++;
		k++;
	}

	for (int i = 0; i < k; i++)
		a[low + i] = tempArray[i];

	delete(tempArray);
}

// Merge Sort: high = n-1, low = 0
template <class T>
void MergeSort(T* a, int high, int low = 0)
{
	if (low >= high)
		return;

	int mid = (high + low) / 2;

	MergeSort(a, mid, low);
	MergeSort(a, high, mid + 1);

	Merge(a, low, mid, high);
}

// Quicksort: high = n-1, low = 0
template <class T>
void Quicksort(T* a, int high, int low = 0)
{
	if (low >= high)
		return;

	int mid = (high + low) / 2;

	if (mid == low)
	{
		if (a[low] > a[high])
			Swap(a[low], a[high]);
		return;
	}

	Swap(a[mid], a[high]);

	int i = low;
	int j = high - 1;


	while (i < j)
	{
		if (a[i] > a[high] && a[j] < a[high])
		{
			Swap(a[i], a[j]);
		}
		else if (a[i] <= a[high])
		{
			i++;
		}
		else if (a[j] >= a[high])
		{
			j--;
		}
	}

	if (i == high - 1)
	{
		if (a[i] > a[high])
			Swap(a[i], a[high]);
	}
	else
		Swap(a[i], a[high]);

	Quicksort(a, i, low);
	Quicksort(a, high, i + 1);
}

// HeapSort helper function
template <class T>
void Heapify(T* a, int size, int i)
{
	int left = i * 2 + 1;
	int right = i * 2 + 2;

	int max = i;

	if (left < size && a[left] > a[i])
		max = left;
	if (right<size && a[right] > a[max])
		max = right;

	if (max != i)
	{
		Swap(a[i], a[max]);
		Heapify(a, size, max);
	}
}

// HeapSort helper function
template <class T>
void BuildMaxHeap(T* a, int size)
{
	for (int i = (size / 2) - 1; i >= 0; i--)
		Heapify(a, size, i);
}

template <class T>
void HeapSort(T* a, int size)
{
	BuildMaxHeap(a, size);
	while (size > 0)
	{
		Swap(a[0], a[size - 1]);
		size--;
		Heapify(a, size, 0);
	}
}

void CountingSort(int* a, int size, int maxValue)
{
	std::vector<int> counter(maxValue+1, 0);

	for (int i = 0; i < size; i++)
	{
		counter[a[i]]++;
	}
	int j = 0;
	for (int i = 0; i < maxValue; i++)
	{
		while (counter[i] != 0)
		{
			a[j] = i;
			j++;
			counter[i]--;
		}
	}
}
