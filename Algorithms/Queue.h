#pragma once
#include <vector>

template <class T>
class Queue
{
private:
	std::vector<T> queue;
public:
	void Enqueue(T item);
	T Dequeue();
	int Size();
};

template<class T>
void Queue<T>::Enqueue(T item)
{
	queue.push_back(item);
}

template<class T>
T Queue<T>::Dequeue()
{
	if (queue.size() == 0)
		return NULL;
	T temp = queue[0];
	queue.erase(queue.begin());
	return temp;
}

template<class T>
int Queue<T>::Size()
{
	return queue.size();
}

class QueueLinkedList
{
private:
	struct Node
	{
		int value;
		Node* next;
	};

	Node* head;
	Node* tail;
	int size;
public:
	QueueLinkedList() { head = nullptr; tail = nullptr; size = 0; }
	void Enqueue(int item);
	int Dequeue();
	int Size();
};

void QueueLinkedList::Enqueue(int item)
{
	Node* temp = new Node;
	temp->next = nullptr;
	temp->value = item;

	if (head == nullptr)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}

	size++;
}

int QueueLinkedList::Dequeue()
{
	if (head == nullptr)
		return NULL;

	Node* temp = head;
	int value = head->value;

	head = head->next;
	if (head == nullptr)
	{
		tail = nullptr;
	}

	delete(temp);
	size--;
	return value;
}

int QueueLinkedList::Size()
{
	return size;
}

class PriorityQueue
{
private:
	std::vector<int> pq;
	void Heapify(int i);
public:
	void Enqueue(int item);
	int Dequeue();
	int Size();
};

void PriorityQueue::Heapify(int i)
{
	int left = i * 2 + 1;
	int right = i * 2 + 2;

	int max = i;

	if (left < pq.size() && pq[left] > pq[i])
		max = left;
	if (right < pq.size() && pq[right] > pq[max])
		max = right;

	if (max != i)
	{
		int temp = pq[i];
		pq[i] = pq[max];
		pq[max] = temp;
		Heapify(max);
	}
}

void PriorityQueue::Enqueue(int item)
{
	pq.push_back(item);
	int i = pq.size() - 1;
	int parent = (i - 1) / 2;
	while (i > 0 && pq[i] > pq[parent])
	{
		Heapify(parent);
		i = parent;
		parent = (i - 1) / 2;
	}
}

int PriorityQueue::Dequeue()
{
	if (pq.size() == 0)
		return NULL;
	int value = pq[0];
	pq[0] = pq[pq.size() - 1];
	pq.pop_back();
	Heapify(0);
	return value;
}

int PriorityQueue::Size()
{
	return pq.size();
}