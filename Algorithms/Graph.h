#pragma once
#include <vector>
#include <tuple>
#include <random>
#include <iostream>

class Graph
{
private:
	struct Vertex
	{
		int index;
		bool visited = false;
		std::vector<std::tuple<Vertex*, int>> edges;
		Vertex(int i) { index = i; }
	};

	std::vector<Vertex> vertices;

	void DFSVisit(Vertex& vertex);

public:
	void GenerateGraph(int numberOfVertices, bool directed = false, bool weighted = false);
	void GenerateAcyclicGraph();
	void PrintGraph();

	void ResetGraph();

	void DFS();
	void BFS();
};

inline void Graph::GenerateGraph(int numberOfVertices, bool directed, bool weighted)
{
	for (int i = 0; i < numberOfVertices; i++)
	{
		vertices.push_back({ i });
	}

	for (int i = 0; i < numberOfVertices; i++)
	{
		int j = rand() % (numberOfVertices / 3 + 2);
		j -= vertices[i].edges.size();
		while (j > 0)
		{
			j--;

			int vertexIndex = rand() % numberOfVertices;
			if (i == vertexIndex)
			{
				j++;
				continue;
			}

			bool edgeExists = false;

			for (int y = 0; y < vertices[i].edges.size(); y++)
			{
				if (vertices[vertexIndex].index == std::get<0>(vertices[i].edges[y])->index)
				{
					edgeExists = true;
					break;
				}
			}

			if (!edgeExists)
			{
				int weight = 1;
				if (weighted)
					weight = rand() % 10;

				vertices[i].edges.push_back({ &vertices[vertexIndex], weight });

				if (!directed)
				{
					vertices[vertexIndex].edges.push_back({ &vertices[i], weight });
				}
			}

		}
	}
}


inline void Graph::GenerateAcyclicGraph()
{

}

inline void Graph::PrintGraph()
{
	for (int i = 0; i < vertices.size(); i++)
	{
		std::cout << i << ": ";
		for (int j = 0; j < vertices[i].edges.size(); j++)
		{
			std::cout << std::get<0>(vertices[i].edges[j])->index << ", ";
		}
		std::cout << std::endl;
	}
}

inline void Graph::ResetGraph()
{
	for (auto& vertex : vertices)
	{
		vertex.visited = false;
	}
}

inline void Graph::DFS()
{
	std::cout << "DFS: ";
	for (auto& vertex : vertices)
	{
		if (vertex.visited)
			continue;

		vertex.visited = true;
		std::cout << vertex.index << " ";

		for (auto& edge : vertex.edges)
		{
			DFSVisit(*(std::get<0>(edge)));
		}
	}
	std::cout << std::endl;
}

inline void Graph::DFSVisit(Vertex& vertex)
{
	if (vertex.visited)
		return;

	vertex.visited = true;
	std::cout << vertex.index << " ";

	for (auto& edge : vertex.edges)
	{
		DFSVisit(*(std::get<0>(edge)));
	}
}

inline void Graph::BFS()
{
	std::cout << "BFS: ";

	std::vector<Vertex*> queue;
	for (auto& vertex : vertices)
	{
		if (vertex.visited)
			continue;

		vertex.visited = true;

		queue.push_back(&vertex);

		while (queue.size() > 0)
		{
			for (auto& edge : queue[0]->edges)
			{
				if (!std::get<0>(edge)->visited)
				{
					queue.push_back(std::get<0>(edge));
					std::get<0>(edge)->visited = true;
				}
			}
			std::cout << queue[0]->index << " ";
			queue.erase(queue.begin());
		}
	}
	std::cout << std::endl;
}