#pragma once
#include <iostream>
#include "Queue.h"

class AVLTree
{
private:
	struct Node
	{
		int key;
		Node* left;
		Node* right;
		Node* parent;
		int height;
	};

	Node* root;
	Queue<Node*> queue;

	int Height(Node* node);
	void FixHeight(Node* node);
	void LeftRotate(Node* node);
	void RightRotate(Node* node);
	void Rebalance(Node* node);

	void InOrderTraversal(Node* node);
	void PreOrderTraversal(Node* node);
	void PostOrderTraversal(Node* node);
public:
	AVLTree() { root = nullptr; }
	void InsertElement(int key);
	void CreateAVLTree(int* a, int size);
	void RemoveElement(int key);

	void InOrderTraversal() { std::cout << "InOrderTraversal: "; InOrderTraversal(root); std::cout << std::endl; }
	void PreOrderTraversal() { std::cout << "PreOrderTraversal: "; PreOrderTraversal(root); std::cout << std::endl; }
	void PostOrderTraversal() { std::cout << "PostOrderTraversal: "; PostOrderTraversal(root); std::cout << std::endl; }
	int Height() { return Height(root); }
};

void AVLTree::InsertElement(int key)
{
	Node* node = new Node;
	node->key = key;
	node->left = nullptr;
	node->right = nullptr;
	node->height = 0;

	if (root == nullptr)
	{
		node->parent = nullptr;
		root = node;
		return;
	}
	
	Node* temp = root;
	
	while (temp != nullptr)
	{
		if (key > temp->key)
		{
			if (temp->right == nullptr)
			{
				temp->right = node;
				node->parent = temp;
				break;
			}
			else
			{
				temp = temp->right;
			}
		}
		else
		{
			if (temp->left == nullptr)
			{
				temp->left = node;
				node->parent = temp;
				break;
			}
			else
			{
				temp = temp->left;
			}
		}
	}
	Rebalance(temp);
}

void AVLTree::CreateAVLTree(int* a, int size)
{
	for (int i = 0; i < size; i++)
		InsertElement(a[i]);
}

inline void AVLTree::RemoveElement(int key)
{
	Node* temp = root;

	while (temp != nullptr)
	{
		if (key > temp->key)
			temp = temp->right;
		else if (key < temp->key)
			temp = temp->left;
		else
			break;
	}

	if (temp == nullptr) return;

	if (temp->left == nullptr && temp->right == nullptr)
	{
		if (temp->parent->left == temp)
			temp->parent->left = nullptr;
		else
			temp->parent->right = nullptr;
		FixHeight(temp->parent);
		delete(temp);
	}
	else if (temp->left != nullptr)
	{
		if (temp->parent->left == temp)
			temp->parent->left = temp->left;
		else
			temp->parent->right = temp->left;

		temp->left->parent = temp->parent;
		FixHeight(temp->parent);
		delete(temp);
	}
	else if (temp->right != nullptr)
	{
		if (temp->parent->left == temp)
			temp->parent->left = temp->right;
		else
			temp->parent->right = temp->right;

		temp->right->parent = temp->parent;
		FixHeight(temp->parent);
		delete(temp);
	}
	else
	{
		Node* predecesor = temp->left;

		while (predecesor->right != nullptr)
		{
			predecesor = predecesor->right;
		}

		if (temp->parent->left == temp)
			temp->parent->left = predecesor;
		else
			temp->parent->right = predecesor;

		predecesor->right = temp->right;
		temp->right->parent = predecesor;

		predecesor->parent->right = predecesor->left;
		predecesor->left->parent = predecesor->parent;
		
		Node* leaf = predecesor->left;

		predecesor->parent = temp->parent;

		temp->left->parent = predecesor;
		predecesor->left = temp->left;

		FixHeight(leaf);
		Rebalance(leaf);
		delete(temp);
	}
}

int AVLTree::Height(Node* node)
{
	return (node == nullptr) ? -1 : node->height;
}

void AVLTree::FixHeight(Node* node)
{
	if (node == nullptr) return;

	int newHeight = (Height(node->left) > Height(node->right)) ? Height(node->left) + 1 : Height(node->right) + 1;
	if (newHeight != node->height)
	{
		node->height = newHeight;
		FixHeight(node->parent);
	}
}

void AVLTree::LeftRotate(Node* node)
{
	Node* right = node->right;
	right->parent = node->parent;

	if (node != root)
	{
		if (node->parent->left == node)
			node->parent->left = right;
		else
			node->parent->right = right;
	}
	else
	{
		root = right;
	}

	node->parent = right;
	node->right = right->left;

	if (right->left != nullptr)
		right->left->parent = node;

	right->left = node;

	FixHeight(node);
}

void AVLTree::RightRotate(Node* node)
{
	Node* left = node->left;
	left->parent = node->parent;

	if (node != root)
	{
		if (node->parent->left == node)
			node->parent->left = left;
		else
			node->parent->right = left;
	}
	else
	{
		root = left;
	}

	node->parent = left;
	node->left = left->right;

	if (left->right != nullptr)
		left->right->parent = node;

	left->right = node;

	FixHeight(node);
}

void AVLTree::Rebalance(Node* node)
{
	if (node == nullptr) return;

	int heightDifference = Height(node->left) - Height(node->right);

	if (heightDifference > 1) // left dominant
	{
		if (Height(node->left->left) > Height(node->left->right)) // left left
		{
			RightRotate(node);
		}
		else // left right
		{
			LeftRotate(node->left);
			RightRotate(node);
		}
	}
	else if (heightDifference < -1) // right dominant
	{
		if (Height(node->right->right) > Height(node->right->left)) // right right
		{
			LeftRotate(node);
		}
		else // right left
		{
			RightRotate(node->right);
			LeftRotate(node);
		}
	}
	else
	{
		FixHeight(node);
	}

	Rebalance(node->parent);
}

void AVLTree::InOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	InOrderTraversal(node->left);
	std::cout << node->key << " ";
	InOrderTraversal(node->right);
}

void AVLTree::PreOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	std::cout << node->key << " ";
	PreOrderTraversal(node->left);
	PreOrderTraversal(node->right);
}

void AVLTree::PostOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	PostOrderTraversal(node->left);
	PostOrderTraversal(node->right);
	std::cout << node->key << " ";
}