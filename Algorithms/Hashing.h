#pragma once
#include <string>
#include <vector>

// finding substring using Rolling Hash
bool StringContainsSubstring(std::string t, std::string s)
{
	int p = 524287; // Mersenne prime 2^19 - 1
	int size = s.size();

	int b = 256;
	int baseToSize = 1;

	int rt = 0;
	int rs = 0;

	for (int i = 0; i < size; i++)
	{
		// build initial rolling hash for t
		rt = rt * 256;
		rt += t[i];
		rt = rt % p;

		// build hash for s
		rs = rs * 256;
		rs += s[i];
		rs = rs % p;

		// calculate b^len(s) % p
		baseToSize = (baseToSize * b) % p;
	}

	if (rt == rs)
	{
		// check if equal
		if (s == t.substr(0, size))
		{
			return true;
		}
	}

	for (int i = size; i < t.size(); i++)
	{
		// rs -> ( rs * b - (old * baseToPowerOfSize)mod p + new ) mod p
		rt = (rt * b - (t[i - size] * baseToSize) % p + t[i]) % p;

		if (rt == rs)
		{
			// check if equal
			if (s == t.substr(i - size + 1, i))
			{
				return true;
			}
		}
	}

	return false;
}