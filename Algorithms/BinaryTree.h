#pragma once
#include <iostream>

class BinaryTree
{
private:
	struct Node
	{
		int key;
		Node* left;
		Node* right;
	};
	Node* root;

	void InOrderTraversal(Node* node);
	void PreOrderTraversal(Node* node);
	void PostOrderTraversal(Node* node);

public:
	BinaryTree() { root = nullptr; }

	void CreateBinaryTree(int* a, int size);
	void AddElement(int key);

	void InOrderTraversal() { std::cout << "InOrderTraversal: "; InOrderTraversal(root); std::cout << std::endl; }
	void PreOrderTraversal() { std::cout << "PreOrderTraversal: "; PreOrderTraversal(root); std::cout << std::endl; }
	void PostOrderTraversal() { std::cout << "PostOrderTraversal: "; PostOrderTraversal(root); std::cout << std::endl; }

	int ReturnMin();
	int ReturnMax();

	bool ElementExists(int key);
};

void BinaryTree::CreateBinaryTree(int* a, int size)
{
	for (int i = 0; i < size; i++)
	{
		AddElement(a[i]);
	}
}

void BinaryTree::AddElement(int key)
{
	Node* newNode = new Node;
	newNode->key = key;
	newNode->right = nullptr;
	newNode->left = nullptr;

	if (root == nullptr)
	{
		root = newNode;
		std::cout << "Added: " << key << " as root" << std::endl;
		return;
	}

	Node* temp = root;
	while (temp != nullptr)
	{
		if (key > temp->key)
		{
			if (temp->right == nullptr)
			{
				temp->right = newNode;
				std::cout << "Added: " << key << std::endl;
				return;
			}
			temp = temp->right;
		}
		else
		{
			if (temp->left == nullptr)
			{
				temp->left = newNode;
				std::cout << "Added: " << key << std::endl;
				return;
			}
			temp = temp->left;
		}
	}
}

void BinaryTree::InOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	InOrderTraversal(node->left);
	std::cout << node->key << " ";
	InOrderTraversal(node->right);
}

void BinaryTree::PreOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	std::cout << node->key << " ";
	PreOrderTraversal(node->left);
	PreOrderTraversal(node->right);
}

void BinaryTree::PostOrderTraversal(Node* node)
{
	if (node == nullptr)
		return;

	PostOrderTraversal(node->left);
	PostOrderTraversal(node->right);
	std::cout << node->key << " ";
}

int BinaryTree::ReturnMin()
{
	Node* temp = root;
	if (temp == nullptr)
		return NULL;

	while (temp->left != nullptr)
		temp = temp->left;

	return temp->key;
}

int BinaryTree::ReturnMax()
{
	Node* temp = root;
	if (temp == nullptr)
		return NULL;

	while (temp->right != nullptr)
		temp = temp->right;

	return temp->key;
}

bool BinaryTree::ElementExists(int key)
{
	Node* temp = root;
	while (temp != nullptr)
	{
		if (temp->key == key)
			return true;

		if (key > temp->key)
			temp = temp->right;
		else
			temp = temp->left;
	}
	return false;
}