#pragma once
#include <vector>

template <class T>
class Stack
{
private:
	std::vector<T> stack;
public:
	void Push(T item);
	T Pop();
	T Peek();
	int Size();
};

template <class T>
void Stack<T>::Push(T item)
{
	stack.push_back(item);
}

template <class T>
T Stack<T>::Pop()
{
	if (stack.size() == 0)
		return NULL;
	T element = stack[stack.size() - 1];
	stack.pop_back();
	return element;
}

template <class T>
T Stack<T>::Peek()
{
	if (stack.size() == 0)
		return NULL;
	return stack[stack.size() - 1];
}

template <class T>
int Stack<T>::Size()
{
	return stack.size();
}


class StackLinkedList
{
private:
	struct Node
	{
		int value;
		Node* next;
	};
	Node* head;
	int size;
public:
	StackLinkedList() { head = nullptr; size = 0; }
	void Push(int item);
	int Pop();
	int Peek();
	int Size();
};

void StackLinkedList::Push(int item)
{
	if (head == nullptr)
	{
		head = new Node();
		head->next = nullptr;
		head->value = item;
	}
	else
	{
		Node* temp = new Node;
		temp->next = head;
		temp->value = item;
		head = temp;
	}
	size++;
}

int StackLinkedList::Pop()
{
	if (head == nullptr)
		return NULL;
	Node* temp = head->next;
	int value = head->value;
	delete(head);
	head = temp;
	return value;
}

int StackLinkedList::Peek()
{
	return head->value;
}

int StackLinkedList::Size()
{
	return size;
}
